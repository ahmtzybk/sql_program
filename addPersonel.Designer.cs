﻿namespace SQL_Program
{
    partial class addPersonel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(addPersonel));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.personelMedeniHali = new System.Windows.Forms.Label();
            this.personelSehir = new System.Windows.Forms.ComboBox();
            this.personelBekar = new System.Windows.Forms.RadioButton();
            this.personelEvli = new System.Windows.Forms.RadioButton();
            this.personelMeslek = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.personelMaas = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.personelSoyad = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.personelAd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.personelID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.grafikler = new System.Windows.Forms.Button();
            this.istatistik = new System.Windows.Forms.Button();
            this.temizle = new System.Windows.Forms.Button();
            this.guncelle = new System.Windows.Forms.Button();
            this.sil = new System.Windows.Forms.Button();
            this.kaydet = new System.Windows.Forms.Button();
            this.listele = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.perIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perAdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perSoyadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perSehirDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perMaasDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perDurumDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.perMeslekDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tablePersonelBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.personelDataSet = new SQL_Program.PersonelDataSet();
            this.tablePersonelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.personelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.personelDBBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.personelDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tablePersonelTableAdapter = new SQL_Program.PersonelDataSetTableAdapters.tablePersonelTableAdapter();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablePersonelBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personelDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablePersonelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personelDBBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personelDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.personelMedeniHali);
            this.groupBox1.Controls.Add(this.personelSehir);
            this.groupBox1.Controls.Add(this.personelBekar);
            this.groupBox1.Controls.Add(this.personelEvli);
            this.groupBox1.Controls.Add(this.personelMeslek);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.personelMaas);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.personelSoyad);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.personelAd);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.personelID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 323);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Personel Kayıt";
            // 
            // personelMedeniHali
            // 
            this.personelMedeniHali.AutoSize = true;
            this.personelMedeniHali.Location = new System.Drawing.Point(135, 265);
            this.personelMedeniHali.Name = "personelMedeniHali";
            this.personelMedeniHali.Size = new System.Drawing.Size(164, 19);
            this.personelMedeniHali.TabIndex = 12;
            this.personelMedeniHali.Text = "personelMedeniHali";
            this.personelMedeniHali.Visible = false;
            // 
            // personelSehir
            // 
            this.personelSehir.FormattingEnabled = true;
            this.personelSehir.Location = new System.Drawing.Point(134, 132);
            this.personelSehir.Name = "personelSehir";
            this.personelSehir.Size = new System.Drawing.Size(168, 27);
            this.personelSehir.TabIndex = 4;
            // 
            // personelBekar
            // 
            this.personelBekar.AutoSize = true;
            this.personelBekar.Location = new System.Drawing.Point(231, 202);
            this.personelBekar.Name = "personelBekar";
            this.personelBekar.Size = new System.Drawing.Size(71, 23);
            this.personelBekar.TabIndex = 7;
            this.personelBekar.TabStop = true;
            this.personelBekar.Text = "Bekar";
            this.personelBekar.UseVisualStyleBackColor = true;
            this.personelBekar.CheckedChanged += new System.EventHandler(this.personelBekar_CheckedChanged);
            // 
            // personelEvli
            // 
            this.personelEvli.AutoSize = true;
            this.personelEvli.Location = new System.Drawing.Point(135, 202);
            this.personelEvli.Name = "personelEvli";
            this.personelEvli.Size = new System.Drawing.Size(52, 23);
            this.personelEvli.TabIndex = 6;
            this.personelEvli.TabStop = true;
            this.personelEvli.Text = "Evli";
            this.personelEvli.UseVisualStyleBackColor = true;
            this.personelEvli.CheckedChanged += new System.EventHandler(this.personelEvli_CheckedChanged);
            // 
            // personelMeslek
            // 
            this.personelMeslek.Location = new System.Drawing.Point(134, 231);
            this.personelMeslek.Name = "personelMeslek";
            this.personelMeslek.Size = new System.Drawing.Size(168, 27);
            this.personelMeslek.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(66, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 19);
            this.label7.TabIndex = 3;
            this.label7.Text = "Meslek";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 201);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 19);
            this.label6.TabIndex = 11;
            this.label6.Text = "Medeni Hali";
            // 
            // personelMaas
            // 
            this.personelMaas.Location = new System.Drawing.Point(134, 165);
            this.personelMaas.Name = "personelMaas";
            this.personelMaas.Size = new System.Drawing.Size(168, 27);
            this.personelMaas.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(78, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 19);
            this.label5.TabIndex = 9;
            this.label5.Text = "Maaş";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(83, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 19);
            this.label4.TabIndex = 7;
            this.label4.Text = "Şehir";
            // 
            // personelSoyad
            // 
            this.personelSoyad.Location = new System.Drawing.Point(134, 99);
            this.personelSoyad.Name = "personelSoyad";
            this.personelSoyad.Size = new System.Drawing.Size(168, 27);
            this.personelSoyad.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 19);
            this.label3.TabIndex = 5;
            this.label3.Text = "Soyad";
            // 
            // personelAd
            // 
            this.personelAd.Location = new System.Drawing.Point(134, 66);
            this.personelAd.Name = "personelAd";
            this.personelAd.Size = new System.Drawing.Size(168, 27);
            this.personelAd.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(97, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ad";
            // 
            // personelID
            // 
            this.personelID.Location = new System.Drawing.Point(134, 33);
            this.personelID.Name = "personelID";
            this.personelID.Size = new System.Drawing.Size(168, 27);
            this.personelID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Personel Id";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.grafikler);
            this.groupBox2.Controls.Add(this.istatistik);
            this.groupBox2.Controls.Add(this.temizle);
            this.groupBox2.Controls.Add(this.guncelle);
            this.groupBox2.Controls.Add(this.sil);
            this.groupBox2.Controls.Add(this.kaydet);
            this.groupBox2.Controls.Add(this.listele);
            this.groupBox2.Location = new System.Drawing.Point(385, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(160, 323);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "İşlemler";
            // 
            // grafikler
            // 
            this.grafikler.Location = new System.Drawing.Point(20, 281);
            this.grafikler.Name = "grafikler";
            this.grafikler.Size = new System.Drawing.Size(116, 34);
            this.grafikler.TabIndex = 15;
            this.grafikler.Text = "Grafikler";
            this.grafikler.UseVisualStyleBackColor = true;
            // 
            // istatistik
            // 
            this.istatistik.Location = new System.Drawing.Point(20, 238);
            this.istatistik.Name = "istatistik";
            this.istatistik.Size = new System.Drawing.Size(116, 34);
            this.istatistik.TabIndex = 14;
            this.istatistik.Text = "İstatistik";
            this.istatistik.UseVisualStyleBackColor = true;
            // 
            // temizle
            // 
            this.temizle.Location = new System.Drawing.Point(20, 195);
            this.temizle.Name = "temizle";
            this.temizle.Size = new System.Drawing.Size(116, 34);
            this.temizle.TabIndex = 13;
            this.temizle.Text = "Temizle";
            this.temizle.UseVisualStyleBackColor = true;
            this.temizle.Click += new System.EventHandler(this.temizle_Click);
            // 
            // guncelle
            // 
            this.guncelle.Location = new System.Drawing.Point(20, 152);
            this.guncelle.Name = "guncelle";
            this.guncelle.Size = new System.Drawing.Size(116, 34);
            this.guncelle.TabIndex = 12;
            this.guncelle.Text = "Güncelle";
            this.guncelle.UseVisualStyleBackColor = true;
            this.guncelle.Click += new System.EventHandler(this.guncelle_Click);
            // 
            // sil
            // 
            this.sil.Location = new System.Drawing.Point(20, 109);
            this.sil.Name = "sil";
            this.sil.Size = new System.Drawing.Size(116, 34);
            this.sil.TabIndex = 11;
            this.sil.Text = "Sil";
            this.sil.UseVisualStyleBackColor = true;
            this.sil.Click += new System.EventHandler(this.sil_Click);
            // 
            // kaydet
            // 
            this.kaydet.Location = new System.Drawing.Point(20, 66);
            this.kaydet.Name = "kaydet";
            this.kaydet.Size = new System.Drawing.Size(116, 34);
            this.kaydet.TabIndex = 10;
            this.kaydet.Text = "Kaydet";
            this.kaydet.UseVisualStyleBackColor = true;
            this.kaydet.Click += new System.EventHandler(this.kaydet_Click);
            // 
            // listele
            // 
            this.listele.Location = new System.Drawing.Point(20, 23);
            this.listele.Name = "listele";
            this.listele.Size = new System.Drawing.Size(116, 34);
            this.listele.TabIndex = 9;
            this.listele.Text = "Listele";
            this.listele.UseVisualStyleBackColor = true;
            this.listele.Click += new System.EventHandler(this.listele_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridView1);
            this.groupBox3.Location = new System.Drawing.Point(12, 341);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(787, 210);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kayıtlar";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.perIdDataGridViewTextBoxColumn,
            this.perAdDataGridViewTextBoxColumn,
            this.perSoyadDataGridViewTextBoxColumn,
            this.perSehirDataGridViewTextBoxColumn,
            this.perMaasDataGridViewTextBoxColumn,
            this.perDurumDataGridViewCheckBoxColumn,
            this.perMeslekDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.tablePersonelBindingSource1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 23);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(781, 184);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // perIdDataGridViewTextBoxColumn
            // 
            this.perIdDataGridViewTextBoxColumn.DataPropertyName = "PerId";
            this.perIdDataGridViewTextBoxColumn.HeaderText = "PerId";
            this.perIdDataGridViewTextBoxColumn.Name = "perIdDataGridViewTextBoxColumn";
            this.perIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // perAdDataGridViewTextBoxColumn
            // 
            this.perAdDataGridViewTextBoxColumn.DataPropertyName = "PerAd";
            this.perAdDataGridViewTextBoxColumn.HeaderText = "PerAd";
            this.perAdDataGridViewTextBoxColumn.Name = "perAdDataGridViewTextBoxColumn";
            // 
            // perSoyadDataGridViewTextBoxColumn
            // 
            this.perSoyadDataGridViewTextBoxColumn.DataPropertyName = "PerSoyad";
            this.perSoyadDataGridViewTextBoxColumn.HeaderText = "PerSoyad";
            this.perSoyadDataGridViewTextBoxColumn.Name = "perSoyadDataGridViewTextBoxColumn";
            // 
            // perSehirDataGridViewTextBoxColumn
            // 
            this.perSehirDataGridViewTextBoxColumn.DataPropertyName = "PerSehir";
            this.perSehirDataGridViewTextBoxColumn.HeaderText = "PerSehir";
            this.perSehirDataGridViewTextBoxColumn.Name = "perSehirDataGridViewTextBoxColumn";
            // 
            // perMaasDataGridViewTextBoxColumn
            // 
            this.perMaasDataGridViewTextBoxColumn.DataPropertyName = "PerMaas";
            this.perMaasDataGridViewTextBoxColumn.HeaderText = "PerMaas";
            this.perMaasDataGridViewTextBoxColumn.Name = "perMaasDataGridViewTextBoxColumn";
            // 
            // perDurumDataGridViewCheckBoxColumn
            // 
            this.perDurumDataGridViewCheckBoxColumn.DataPropertyName = "PerDurum";
            this.perDurumDataGridViewCheckBoxColumn.HeaderText = "PerDurum";
            this.perDurumDataGridViewCheckBoxColumn.Name = "perDurumDataGridViewCheckBoxColumn";
            // 
            // perMeslekDataGridViewTextBoxColumn
            // 
            this.perMeslekDataGridViewTextBoxColumn.DataPropertyName = "PerMeslek";
            this.perMeslekDataGridViewTextBoxColumn.HeaderText = "PerMeslek";
            this.perMeslekDataGridViewTextBoxColumn.Name = "perMeslekDataGridViewTextBoxColumn";
            // 
            // tablePersonelBindingSource1
            // 
            this.tablePersonelBindingSource1.DataMember = "tablePersonel";
            this.tablePersonelBindingSource1.DataSource = this.personelDataSet;
            // 
            // personelDataSet
            // 
            this.personelDataSet.DataSetName = "PersonelDataSet";
            this.personelDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tablePersonelBindingSource
            // 
            this.tablePersonelBindingSource.DataMember = "tablePersonel";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(551, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(245, 313);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // personelDataSetBindingSource
            // 
            this.personelDataSetBindingSource.DataSource = this.personelDataSet;
            this.personelDataSetBindingSource.Position = 0;
            // 
            // tablePersonelTableAdapter
            // 
            this.tablePersonelTableAdapter.ClearBeforeFill = true;
            // 
            // addPersonel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 563);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "addPersonel";
            this.Text = "Personel Kayıt";
            this.Load += new System.EventHandler(this.addPersonel_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablePersonelBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personelDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablePersonelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personelDBBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personelDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton personelBekar;
        private System.Windows.Forms.RadioButton personelEvli;
        private System.Windows.Forms.TextBox personelMeslek;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox personelMaas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox personelSoyad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox personelAd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox personelID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button grafikler;
        private System.Windows.Forms.Button istatistik;
        private System.Windows.Forms.Button temizle;
        private System.Windows.Forms.Button guncelle;
        private System.Windows.Forms.Button sil;
        private System.Windows.Forms.Button kaydet;
        private System.Windows.Forms.Button listele;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.BindingSource tablePersonelBindingSource;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox personelSehir;
        private System.Windows.Forms.BindingSource personelBindingSource;
        private System.Windows.Forms.BindingSource personelDBBindingSource;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource personelDataSetBindingSource;
        private PersonelDataSet personelDataSet;
        private System.Windows.Forms.BindingSource tablePersonelBindingSource1;
        private PersonelDataSetTableAdapters.tablePersonelTableAdapter tablePersonelTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn perIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perAdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perSoyadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perSehirDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perMaasDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn perDurumDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perMeslekDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label personelMedeniHali;
    }
}