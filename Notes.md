# SQL Veri Tipleri

- bit: Mant�ksal Veri Tipi (0 veya 1 yada true yada false verileri tutar)
- char(10): Sabit Uzunluklu Veriler (Telefon Numaras�, TC Kimlik Numaras� gibi, Burdaki 10 karakterli)
- decimal(18,0): Virg�ll� Say�lar i�in kullan�l�r, ilk de�er virg�lden �nceki basamak say�s�, ikinci de�er virg�lden sonraki basamak say�s�n� ifade eder
- float: Ondal�kl� de�erler ama decimalden daha az uzunlukta de�erler
- smalldatetime: 1900 ile 2070 y�llar� aras�ndaki tarihleri tutar
- smallint: ToInt16'n�n kar��l���d�r
- tinyint: 0 ile 255 dahil olmak �zere say�lar� tutar
- varchar(50): De�i�ken uzunluklu karakterler, kullan�lmayan karakterleri belle�e geri iade ediyor (char(10) da iade etmez)
- int: Tam say� de�erleri

