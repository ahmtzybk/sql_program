﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient; // SQL Komutlarını kullanmak için gerekli kütüphane

namespace SQL_Program
{
    public partial class addPersonel : Form
    {
        public addPersonel()
        {
            InitializeComponent();
        }

        // Global Alanda SQL Bağlantımızı Sağlıyoruz
        SqlConnection Baglanti = new SqlConnection("Data Source=hyp3ri0nn\\sqlserver;Initial Catalog=sampleDatabase;Integrated Security=True");

        // TextBox'taki değerleri sıfırlama metodu
        void Temizle()
        {
            personelID.Text = "";
            personelAd.Text = "";
            personelSoyad.Text = "";
            personelSehir.Text = "";
            personelMaas.Text = "";
            personelMeslek.Text = "";
            personelEvli.Checked = false;
            personelBekar.Checked = false;
            personelAd.Focus();
        }

        // Form Yüklendiğinde Data Grid'e veriler aktarılıyor
        private void addPersonel_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'personelDataSet.tablePersonel' table. You can move, or remove it, as needed.
            this.tablePersonelTableAdapter.Fill(this.personelDataSet.tablePersonel);
        }

        private void listele_Click(object sender, EventArgs e)
        {
            this.tablePersonelTableAdapter.Fill(this.personelDataSet.tablePersonel);
        }

        // Personel Bilgilerini Kaydetme
        private void kaydet_Click(object sender, EventArgs e)
        {
            Baglanti.Open(); // Bağlantıyı Açıyoruz

            SqlCommand komut = new SqlCommand("INSERT INTO tablePersonel (PerAd,PerSoyad,PerSehir,PerMaas,PerDurum,PerMeslek) VALUES (@ad,@soyad,@sehir,@maas,@medenihali,@meslek)",Baglanti);

            komut.Parameters.AddWithValue("@ad", personelAd.Text);
            komut.Parameters.AddWithValue("@soyad", personelSoyad.Text);
            komut.Parameters.AddWithValue("@sehir", personelSehir.Text);
            komut.Parameters.AddWithValue("@maas", personelMaas.Text);
            komut.Parameters.AddWithValue("@meslek", personelMeslek.Text);
            komut.Parameters.AddWithValue("@medenihali", personelMedeniHali.Text);

            komut.ExecuteNonQuery(); // Sorguyu Çalıştırma >> INSERT UPDATE ve DELETE sorgularında kullanılır

            Baglanti.Close(); // Bağlantıyı Kapatıyoruz

            this.tablePersonelTableAdapter.Fill(this.personelDataSet.tablePersonel); // Kayıt Sonrası Tabloyu Güncelleme
            
            // MessageBox.Show("Personel Eklendi");
        }

        // Silme
        private void sil_Click(object sender, EventArgs e)
        {
            Baglanti.Open();

            SqlCommand Sil = new SqlCommand("DELETE FROM tablePersonel WHERE PerID=@personelID", Baglanti);

            Sil.Parameters.AddWithValue("@personelID", personelID.Text);
            Sil.ExecuteNonQuery();

            Baglanti.Close();

            this.tablePersonelTableAdapter.Fill(this.personelDataSet.tablePersonel); // Silme İşlemi Sonrası Tabloyu Güncelleme

            // MessageBox.Show("Kayıt Silindi");
        }

        private void personelEvli_CheckedChanged(object sender, EventArgs e)
        {
            personelMedeniHali.Text = "True";
        }

        private void personelBekar_CheckedChanged(object sender, EventArgs e)
        {
            personelMedeniHali.Text = "False";
        }

        private void temizle_Click(object sender, EventArgs e)
        {
            Temizle();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int aktifSatir = dataGridView1.SelectedCells[0].RowIndex;

            personelID.Text = dataGridView1.Rows[aktifSatir].Cells[0].Value.ToString();
            personelAd.Text = dataGridView1.Rows[aktifSatir].Cells[1].Value.ToString();
            personelSoyad.Text = dataGridView1.Rows[aktifSatir].Cells[2].Value.ToString();
            personelSehir.Text = dataGridView1.Rows[aktifSatir].Cells[3].Value.ToString();
            personelMaas.Text = dataGridView1.Rows[aktifSatir].Cells[4].Value.ToString();
            personelMeslek.Text = dataGridView1.Rows[aktifSatir].Cells[6].Value.ToString();

            // Radio Butonların Kontrolünü Yaptırıyoruz
            personelMedeniHali.Text = dataGridView1.Rows[aktifSatir].Cells[5].Value.ToString();

            if (personelMedeniHali.Text == "True")
            {
                personelEvli.Checked = true;
            }
            else
            {
                personelBekar.Checked = true;
            }
        }

        private void guncelle_Click(object sender, EventArgs e)
        {
            Baglanti.Open();

            SqlCommand Guncelle =
                new SqlCommand(
                    "UPDATE tablePersonel SET PerAd=@PerAd, PerSoyad=@PerSoyad, PerSehir=@PerSehir, PerMaas=@PerMaas, PerDurum=@PerDurum, PerMeslek=@PerMeslek WHERE PerId=@PerID",
                    Baglanti);

            Guncelle.Parameters.AddWithValue("@PerAd", personelAd.Text);
            Guncelle.Parameters.AddWithValue("@PerSoyad", personelSoyad.Text);
            Guncelle.Parameters.AddWithValue("@PerSehir", personelSehir.Text);
            Guncelle.Parameters.AddWithValue("@PerMaas", personelMaas.Text);
            Guncelle.Parameters.AddWithValue("@PerDurum", personelMedeniHali.Text);
            Guncelle.Parameters.AddWithValue("@PerMeslek", personelMeslek.Text);

            // Güncellenecek Satır
            Guncelle.Parameters.AddWithValue("@PerID", personelID.Text);

            Guncelle.ExecuteNonQuery();

            // Güncelleme İşlemi Sonrası Tabloyu Güncelleme
            this.tablePersonelTableAdapter.Fill(this.personelDataSet.tablePersonel);

            Baglanti.Close();
            // Message.Show("Personel Güncellendi");
        }
    }
}
